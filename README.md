# LanceLeau

Repository recensant l'ensemble des fichiers nécessaire au bon fonctionnement de Lance L'eau.

# Installation et utilisation

Créer un dossier dans lequel nous allons mettre tout nos fichiers, audio, modèles ...

## Créer et activer un environnement virtuel python 

```
sudo apt install virtualenv
virtualenv -p python3 $HOME/tmp/deepspeech-venv/
source $HOME/tmp/deepspeech-venv/bin/activate
```


## Installer DeepSpeech

`pip install deepspeech==0.6.0`

## Récupération et extraction du modèle Francais de pré-entrainé de CommonVoice

```
mkdir model-fr
cd model-fr
wget https://github.com/Common-Voice/commonvoice-fr/releases/download/v0.6.0-fr-0.4/model_tensorflow_fr.tar.xz
tar xvf model_tensorflow_fr.tar.xz
```
Une fois les fichiers extrais, vous pouvez supprimer l'archive téléchargée.

## Transcrire l'audio

Revenir à la racine du projet

`deepspeech --model model-fr/output_graph.pbmm --audio path/to/audio`
`python3 DeepSpeech-examples/mic_vad_streaming/mic_vad_streaming.py --model lanceleau/model-fr/output_graph.tflite -l lanceleau/model-fr/lm.binary -t lanceleau/model-fr/trie -v 3 -w save-audio/`


## Lancement du script au démarrage

Pour changer le nom de la raspberry correspondante à un utilisateur, il faut modifier la valeur de la variable HOSTNAME au début du script `start.sh`

Editer le fichier de `/etc/local.rc` et y ajouter la commande suivante :
`/bin/bash /home/pi/voiceproject/lanceleau/start.sh`

Après chaque démarrage ou redémarrage de la raspberry, le script s'exécutera et lancera le client Java suivi de deepspeech. Les deux en processus de fond avec les logs disponibles aux deux processus dans le répertoire `/home/pi`
