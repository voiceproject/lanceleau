#! /bin/bash

echo "##### VERIFICATION ET INITIALISATION DU USERNAME #####"

export USERNAME="Utilisateur1"

echo "##### FIN DE LA VERIFICATION ET INITIALISATION DU USERNAME #####"

function checkItJava()
{
 ps auxw | grep -P '\b'$1'(?!-)\b' >/dev/null
 if [ $? != 0 ]
 then
   echo "Java client down"
   cd /home/pi/voiceproject/websockets/Client/target
   java -jar JAVAClientWebSocket-v1.jar  &> /home/pi/java_client.log &
   echo "Client java running ..."
 else
   echo "Java client deja up"
 fi;
}

function checkItLanceleau()
{
 ps auxw | grep -P '\b'$1'(?!-)\b' >/dev/null
 if [ $? != 0 ]
 then
   echo "Lanceleau est down"
   cd /home/pi/voiceproject/
   echo "Lanceleau running ...."
   python3 lanceleau/mic_vad_streaming/mic_vad_streaming.py --model lanceleau/model-fr/output_graph.tflite -l lanceleau/model-fr/lm.binary -t lanceleau/model-fr/trie -v 3 -w websockets/Client/fileToSend/  &> /home/pi/lanceleau.log &
 else
   echo "Lanceleau est deja up"
 fi;
}

echo "##### VERFICIATION ET INSTALLATION DU SERVICE JAVA-CLIENT #####"
dpkg -s "default-jdk" &> /dev/null

if [ $? -eq 0 ]; then
    echo "Package default-jdk is already installed!"
else
    echo "Package default-jdk is NOT installed, we installing this ..."
    sudo apt install -y default-jdk
fi

checkItJava "java"
sleep 10


echo "##### DEBUT VERIFICATION ET INSTALLATION DES PACKAGES #####"
dpkg -s "virtualenv" &> /dev/null

if [ $? -eq 0 ]; then
    echo "Package virtualenv is already installed!"
else
    echo "Package virtualenv is NOT installed, we installing ..."
    sudo apt install -y virtualenv
    sudo apt install -y python-dev libatlas-base-dev
fi

dpkg -s "portaudio19-dev" &> /dev/null

if [ $? -eq 0 ]; then
    echo "Package portaudio19-dev is already installed"
else
    echo "Package portaudio19-dev is NOT installed, we installing this ..."
    sudo apt install -y portaudio19-dev
fi
echo "##### FIN VERIFICATION ET INSTALLTION DES PACKAGES #####"

echo "##### DEBUT VERIFICATION ET INSTALLATION VIRTUALENV #####"
if [ ! -d ~/tmp/deepspeech-venv/ ]; then
   echo "L'environnement virtuel pour DeepSpeech n'existe pas"
   virtualenv -p python3 ~/tmp/deepspeech-venv/
   echo "Entrée dans l'environnement viruel de DeepSpeech ..."
   source ~/tmp/deepspeech-venv/bin/activate
   pip install requests
else
   echo "L'environnement virtuel pour DeepSpeech existe"
   echo "Entrée dans l'environnement virtuel de DeepSpeech ..."
   source ~/tmp/deepspeech-venv/bin/activate
fi
echo "##### FIN VERIFICATION ET INSTALLATION VIRTUALENV #####"

cd /home/pi/voiceproject/lanceleau/
if [ ! -d model-fr/ ]; then
    echo "Le répertoire des models FR n'existe pas"
    mkdir model-fr
    cd model-fr
    wget https://github.com/Common-Voice/commonvoice-fr/releases/download/v0.6.0-fr-0.4/model_tflite_fr.tar.xz
    tar xvf model_tflite_fr.tar.xz
    rm -rf model_tflite_fr.tar.xz
else
    echo "Le répertoire des models FR existe déjà"
fi

cd /home/pi/voiceproject/lanceleau/
    pip install -r mic_vad_streaming/requirements.txt

cd /home/pi/voiceproject/
if [ ! -d websockets ]; then
    echo "Le repository des websockets n'a pas était cloner"
    git clone https://gitlab.com/voiceproject/websockets.git
else
    echo "Le repository des websockets est présent, récupération de sa dernière version ..."
    cd websockets/
    git pull
fi

checkItLanceleau "lanceleau"

exit 0;